//
//  DataCacheManager.swift
//  MeteoApp
//
//  Created by mac on 26/6/2023.
//

import Foundation

class DataCacheManager : NSObject
{
    
    static let sharedInstance = DataCacheManager()
    // Checking the UserDefaults is saved or not
    func didSave(preferences: UserDefaults){
        let didSave = preferences.synchronize()
        if !didSave{
            // Couldn't Save
            print("Preferences could not be saved!")
        }
    }
    func saveCitys(data: [MeteoInfoResponse]) {
        let preferences = UserDefaults.standard
        preferences.set(try? PropertyListEncoder().encode(data), forKey: METEO_DATA_KEY)
        didSave(preferences: preferences)
    }
    
    func getCitys()->[MeteoInfoResponse]
    {
        var mData:[MeteoInfoResponse]
        if let data = UserDefaults.standard.value(forKey: METEO_DATA_KEY) as? Data {
            let datas = try? PropertyListDecoder().decode([MeteoInfoResponse].self, from: data)
            mData = datas! as [MeteoInfoResponse]
        }else
        {
            mData = [MeteoInfoResponse]()
        }
        return mData
    }
    
    
}
