//
//  Int.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Foundation
extension Int {
    func secondsToHoursMinutesSeconds() -> String {
        let dcf = DateComponentsFormatter()
        dcf.allowedUnits = [.hour, .minute, .second]
        dcf.unitsStyle = .positional
        dcf.zeroFormattingBehavior = .pad
        return dcf.string(from: TimeInterval(self)) ?? ""
    }
}
