//
//  AddCityCoordinator.swift
//  MeteoApp
//
//  Created by mac on 26/6/2023.
//

import Foundation
import RxSwift

class AddCtiyCoordinator: BaseCoordinator {
    
    private let viewModel: AddCtiyViewModel
    private let disposeBag = DisposeBag()
    
    lazy var apiClient: MeteoAppService = {
        return MeteoAppService.shared
    }()
    
    
    init(viewModel: AddCtiyViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        // Start Home View
        let viewController = AddCityViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.pushViewController(viewController, animated: true)
        setUpBindings()
    }
    
    private func setUpBindings() {
        viewModel.didTapAddCity
            .subscribe(onNext: { [weak self] cityName in
                // Got to city meteo Info
                self?.showCtiyMeteo(cityName)
            })
            .disposed(by: disposeBag)
    }
    
    func showCtiyMeteo(_ cityName: String) {
        // Init city Info Coordinator
        let city = MeteoInfoResponse(coord: nil, weather: nil, base: nil, main: nil, visibility: nil, wind: nil, clouds: nil, dt: nil, sys: nil, timezone: nil, id: nil, name: cityName, cod: nil)
        let coordinator = CityMeteoCoordinator(viewModel: CityMeteoViewModel(service: apiClient, city: city))
        coordinator.navigationController = navigationController
        
        start(coordinator: coordinator)
    }
    
}
