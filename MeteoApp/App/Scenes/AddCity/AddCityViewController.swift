//
//  AddCityViewController.swift
//  MeteoApp
//
//  Created by mac on 26/6/2023.
//

import UIKit

class AddCityViewController: UIViewController, Storyboarded {
    static var storyboard = AppStoryboard.main

    @IBOutlet weak var mCityNameTF: UITextField!
    
    var viewModel: AddCtiyViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func didTouchAddBT(_ sender: Any) {
        if let cityName = mCityNameTF.text, !cityName.isEmpty, cityName.count > 3 {
            viewModel?.didTapAddCity.onNext(cityName.trimmingCharacters(in: .whitespaces))
        }
    }
    

}
