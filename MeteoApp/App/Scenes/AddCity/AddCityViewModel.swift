//
//  AddCityViewModel.swift
//  MeteoApp
//
//  Created by mac on 26/6/2023.
//

import RxSwift
import UIKit

class AddCtiyViewModel {
    fileprivate let disposeBag = DisposeBag()
    private let mMeteoAppService: MeteoApp
    
    // events
    let didTapAddCity = PublishSubject<String>()

    
    init(service: MeteoApp) {
        self.mMeteoAppService = service
    }
}
