//
//  HomeViewController.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController, Storyboarded {
    
    static var storyboard = AppStoryboard.main
    
    
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    var mCitysLists : [MeteoInfoResponse] = []
    
    private let disposeBag = DisposeBag()
    
    var viewModel: HomeViewModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Show city in 3 grid 3 columns
        mCollectionView.collectionViewLayout = GridLayout(numberOfColumns: 3, heightCell: 228, spacing: 5)
        setUpBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        // Fetch citys
        viewModel?.getCitysLists()
    }
    
    private func setUpBindings() {

        viewModel?.citysListsResponse
            .subscribe { result in

                if let citysLists = result.element, !citysLists.isEmpty {
                    self.mCitysLists = citysLists
                    self.mCollectionView.reloadData()
                } else {
                        self.viewModel?.addNewCity.onNext(true)
                    
                }
            }.disposed(by: disposeBag)

       
    }

    @IBAction func touchAddCity(_ sender: Any) {
        viewModel?.addNewCity.onNext(true)
    }
    
}




extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCell", for: indexPath) as! CityCell
        cell.initCell(city: self.mCitysLists[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mCitysLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel?.didSelectCity.onNext(self.mCitysLists[indexPath.row])
    }
}


