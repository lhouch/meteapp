//
//  CityCell.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import UIKit

class CityCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }
    
    func initCell(city : MeteoInfoResponse){
        self.title.text = city.name
    }
    
   func configUI(){
       self.viewContent.layer.cornerRadius = 5
       self.viewContent.layer.masksToBounds = true
       self.viewContent.layer.borderColor = UIColor.black.cgColor
       self.viewContent.layer.borderWidth = 1
       
       self.viewContent.backgroundColor = UIColor.random()
    }
}
