//
//  HomeViewModel.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import RxSwift
import UIKit

class HomeViewModel {
    
    fileprivate let disposeBag = DisposeBag()
    private let mMeteoAppService: MeteoApp
    
    // events
    let citysListsResponse = PublishSubject<[MeteoInfoResponse]>()
    
    let didSelectCity = PublishSubject<MeteoInfoResponse>()
    let addNewCity = PublishSubject<Bool>()

    
    
    
    init(service: MeteoApp) {
        self.mMeteoAppService = service
    }
    
    func getCitysLists() -> Void {
        let listData = DataCacheManager.sharedInstance.getCitys()
        self.citysListsResponse.onNext(listData)
    }
}

