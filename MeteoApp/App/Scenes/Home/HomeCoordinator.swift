//
//  HomeCoordinator.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Foundation
import RxSwift

class HomeCoordinator: BaseCoordinator {
    
    private let viewModel: HomeViewModel
    private let disposeBag = DisposeBag()
    
    lazy var apiClient: MeteoAppService = {
        return MeteoAppService.shared
    }()
    
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        // Start Home View
        let viewController = HomeViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.viewControllers = [viewController]
        
        setUpBindings()
    }
    
    func showMeteoCity(_ city: MeteoInfoResponse) {
        // Init City Info Coordinator
        let coordinator = CityMeteoCoordinator(viewModel: CityMeteoViewModel(service: apiClient, city: city))
        coordinator.navigationController = navigationController
        
        start(coordinator: coordinator)
    }
    
    func showAddNewCity(_ listesIsEmpty: Bool){
        // Init City Info Coordinator
        let coordinator = AddCtiyCoordinator(viewModel: AddCtiyViewModel(service: apiClient))
        coordinator.navigationController = navigationController

        start(coordinator: coordinator)
    }
    
    
    private func setUpBindings() {
        viewModel.addNewCity
            .subscribe(onNext: { [weak self] isEmpty in
                // Got to add
                self?.showAddNewCity(isEmpty)
            })
            .disposed(by: disposeBag)
        
        viewModel.didSelectCity
            .subscribe(onNext: { [weak self] city in
                // Got Info
                self?.showMeteoCity(city)
            })
            .disposed(by: disposeBag)
    }
}
