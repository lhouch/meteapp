//
//  CityMeteoViewController.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import UIKit
import RxSwift

class CityMeteoViewController: UIViewController, Storyboarded {
    static var storyboard = AppStoryboard.main
    
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var param1: UILabel!
    @IBOutlet weak var value1: UILabel!
    @IBOutlet weak var param2: UILabel!
    @IBOutlet weak var value2: UILabel!
    @IBOutlet weak var param3: UILabel!
    @IBOutlet weak var value3: UILabel!
    
    var viewModel: CityMeteoViewModel?
    private let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        setUpBindings()
    }
    
    func initUI(_ meteoListInfo: MeteoInfoResponse) {
        iconIV.image = UIImage(named: meteoListInfo.weather![0].icon)
        param1.text = "City name"
        value1.text = meteoListInfo.name
        param2.text = "Description"
        value2.text = meteoListInfo.weather![0].description
        param3.text = "Visibilité"
        value3.text = "\(meteoListInfo.visibility!)"
    }
    
    
    private func setUpBindings() {
        
        viewModel?.infoList
            .subscribe { result in
                
                self.initUI(result.element!)
                self.hideActivityIndicatoryInSuperview()
                
            }.disposed(by: disposeBag)
        
        
        viewModel?.responseError.subscribe{error in
            self.hideActivityIndicatoryInSuperview()            
            self.showAlertViewInSuperview("Error", message: error.element!.localizedDescription) {
                print(error)
            }
        }.disposed(by: disposeBag)
        
        viewModel?.isLoading
            .subscribe { isLoading in
                if isLoading.element! {
                    self.showActivityIndicatoryInSuperview()
                }
            }.disposed(by: disposeBag)
        
    }
    
}
