//
//  CityMeteoViewModel.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import RxSwift
import UIKit

class CityMeteoViewModel {
    
    fileprivate let disposeBag = DisposeBag()
    private let mMeteoAppService: MeteoApp
    let city: MeteoInfoResponse?
    let isLoading = BehaviorSubject(value: true)
    let infoList = PublishSubject<MeteoInfoResponse>()
    let responseError = PublishSubject<Error>()
    
    
    
    
    
    init(service: MeteoApp, city: MeteoInfoResponse) {
        self.mMeteoAppService = service
        self.city = city
        getMeteoListInfo()
    }
    
    func getMeteoListInfo() -> Void {
        isLoading.onNext(true)
        self.mMeteoAppService
            .fetchCityMeteoInfo(cityName: self.city!.name!)
            .subscribe { result in
                self.infoList.onNext(result)
                self.saveCity(result)
            } onError: { error in
                self.responseError.onNext(error)
            }
            .disposed(by: disposeBag)
      
    }
    
    
    func saveCity(_ city: MeteoInfoResponse){
        var listData = DataCacheManager.sharedInstance.getCitys()
        
        if (listData.first(where: { $0.name == city.name }) != nil){
            print("City")
        } else {
            listData.append(city)
            DataCacheManager.sharedInstance.saveCitys(data: listData)
        }
      
    }
 
    
}
