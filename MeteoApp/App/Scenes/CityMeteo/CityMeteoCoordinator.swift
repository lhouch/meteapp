//
//  CityMeteoCoordinator.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Foundation

class CityMeteoCoordinator: BaseCoordinator {
    
    private let viewModel: CityMeteoViewModel
    
    init(viewModel: CityMeteoViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        let viewController = CityMeteoViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.pushViewController(viewController, animated: true)
        setUpBindings()
    }
    
    private func setUpBindings() {
//        viewModel.
//            .subscribe(onNext: { [weak self] isEmpty in
//                // Got to infoList Info
//                self?.showAddNewCity(isEmpty)
//            })
//            .disposed(by: disposeBag)
    }
    
    
}
