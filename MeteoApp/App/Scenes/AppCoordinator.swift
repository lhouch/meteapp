//
//  AppCoordinator.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//


import UIKit

class AppCoordinator: BaseCoordinator {
    
    // MARK: - Properties
    let window: UIWindow?
    
    // MARK: - Coordinator
    init(window: UIWindow?) {
        self.window = window
    }
    
    lazy var apiClient: MeteoAppService = {
        return MeteoAppService.shared
    }()
    
    
    override func start() {
        guard let window = window else {
            return
        }
        
        
        window.makeKeyAndVisible()
        
        showHome()
    }
    
    private func showHome() {
        removeChildCoordinators()
        let coordinator = HomeCoordinator(viewModel: HomeViewModel(service: apiClient))
        coordinator.navigationController = BaseNavigationController()
        start(coordinator: coordinator)
        
        ViewControllerUtils.setRootViewController(
            window: window!,
            viewController: coordinator.navigationController,
            withAnimation: true)
    }
}
