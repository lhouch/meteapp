//
//  Global.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Foundation

/*****************Object Key***********************/
var ActivityIndicatorViewInSuperviewAssociatedObjectKey = "_UIViewActivityIndicatorViewInSuperviewAssociatedObjectKey";
var METEO_DATA_KEY = "meteo_data_key"
