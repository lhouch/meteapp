//
//  MetroAppService.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import RxSwift
import Alamofire

class MeteoAppService {
    private lazy var httpService = MeteoAppHttpService()
    static let shared: MeteoAppService = MeteoAppService()
}

extension MeteoAppService: MeteoApp {
    
    
    func fetchCityMeteoInfo(cityName: String) -> Single<MeteoInfoResponse> {
        return Single.create { [httpService] (single) -> Disposable in
            
            do {
                try MeteoAppHttpRouter.getMeteoListInfo(cityName)
                    .request(usingHttpService: httpService)
                    .responseJSON { (result) in
                        do {
                            let meteoInfo = try MeteoAppService.parseCityMeteoInfo(result: result)
                            single(.success(meteoInfo))
                        } catch {
                            single(.error(error))
                        }
                    }
            } catch {
                single(.error(CustomError.error(message: "fetch failed")))
            }
            
            return Disposables.create()
        }
    }
    
    

}

extension MeteoAppService {
    
    static func parseCityMeteoInfo(result: AFDataResponse<Any>) throws -> MeteoInfoResponse {
        
        guard
            let data = result.data,
            let meteoInfoResponse = try? JSONDecoder().decode(MeteoInfoResponse.self, from: data)
        else {
            throw CustomError.error(message: "Invalid Data")
        }
        return meteoInfoResponse
    }
    
    
}
