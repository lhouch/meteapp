//
//  MeteoAppHttpRouter.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Alamofire

/*****************API Key***********************/
let  USER_ID = "f6251b44f363b648bda01497b2174312"
let  LIST_METEO_INFO_URL = "data/2.5/weather?q=%@&APPID=\(USER_ID)"


enum MeteoAppHttpRouter {
    case getMeteoListInfo(String)
}

extension MeteoAppHttpRouter: HttpRouter{
    
    var baseUrlString: String {
        return Bundle.main.object(forInfoDictionaryKey: "BaseUrL") as! String
    }
    
    var path: String {
        switch self {
        case .getMeteoListInfo(let cityName):
            return String(format: LIST_METEO_INFO_URL, cityName)
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getMeteoListInfo:
            return .get
        }
    }
    
    var parammeters: Parameters? {
        switch self {
        case .getMeteoListInfo:
            return nil
        }
    }
    
    func body() throws -> Data? {
        return nil
    }
}
