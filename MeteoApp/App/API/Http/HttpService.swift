//
//  HttpService.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Alamofire

protocol HttpService {
    var sessionManager: Session {get set}
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest
}
