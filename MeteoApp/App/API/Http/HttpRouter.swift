//
//  HttpRouter.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Alamofire

protocol HttpRouter{
    var baseUrlString: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var parammeters: Parameters? { get }
    func body() throws -> Data?
    func request(usingHttpService service: HttpService) throws -> DataRequest
}

extension HttpRouter {
    var parammeters: Parameters? { return nil }
    func body() throws -> Data? { return nil }
    var headers: HTTPHeaders? { return nil }
    func asUrlRequest() throws -> URLRequest {
        
        let urlString = "\(baseUrlString)\(path)"
        let url = try urlString.asURL()
        
        var request = try URLRequest(url: url, method: method, headers: headers)
        request.httpBody = try body()
        return request
    }
    
    func request(usingHttpService service: HttpService) throws -> DataRequest {
        return try service.request(asUrlRequest())
    }
}
