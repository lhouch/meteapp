//
//  MeteoAppAPI.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import RxSwift

protocol MeteoApp {
    func fetchCityMeteoInfo(cityName: String) -> Single<MeteoInfoResponse>

}
