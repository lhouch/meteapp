//
//  MeteoAppHttpService.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

import Alamofire

class MeteoAppHttpService: HttpService {
  
    var sessionManager: Session = Session.default
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest {
        return sessionManager.request(urlRequest).validate(statusCode: 200..<400)
    }
}
