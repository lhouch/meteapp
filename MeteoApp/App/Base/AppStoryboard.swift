//
//  AppStoryboard.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//
import Foundation

enum AppStoryboard: String {
    case main = "Main"
}
