//
//  CustomError.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//

enum CustomError: Error {
    case error(message: String)
}
