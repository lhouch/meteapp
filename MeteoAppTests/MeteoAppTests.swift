//
//  MeteoAppTests.swift
//  MeteoApp
//
//  Created by Lhouch on 26/06/2023.
//


import XCTest
@testable import MeteoApp
import RxSwift

class MeteoAppTests: XCTestCase {
    

    private var disposeBag:DisposeBag?
    private var homeViewModel: HomeViewModel?
    private var cityMeteoViewModel: CityMeteoViewModel?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    override func setUp() {
        super.setUp()
        
        self.disposeBag = DisposeBag()
        let apiClientShared = MeteoAppService.shared

        
        // Init HomeViewModel get city
        self.homeViewModel = HomeViewModel(service: apiClientShared)
        self.homeViewModel?.getCitysLists()

        // Init CityMeteoViewModel
        let cityExmple = MeteoInfoResponse(coord: nil, weather: nil, base: nil, main: nil, visibility: nil, wind: nil, clouds: nil, dt: nil, sys: nil, timezone: nil, id: 123124, name: "Paris", cod: 87)
        self.cityMeteoViewModel = CityMeteoViewModel(service: apiClientShared, city: cityExmple)
        self.cityMeteoViewModel?.getMeteoListInfo()
        
        
        self.setUpBindings()
        
    }
    
    
    private func setUpBindings() {
        
        self.homeViewModel?.citysListsResponse
            .subscribe { result in
                    print(result)
            } onError: { error in
                print(error.localizedDescription)
            }.disposed(by: self.disposeBag!)

       
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
